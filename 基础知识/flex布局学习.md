# flex布局学习

### 一、flex布局原理

flex: flexible box的缩写，意为弹性布局，用来为盒状模型提供最大的灵活性，任何一个容器都可以指定为flex布局。

- 当我们为父盒子设为flex布局以后，子元素的float、clear和vertical-align属性将失效；
- 伸缩布局=弹性布局=伸缩盒布局=弹性布局=flex布局

#### 1.1 布局原理

采用flex布局的元素，称为flex**容器**。

它的所有子元素自动成为容器成员，称为flex项目，简称**项目**。

### 二、flex布局父项常见属性

以下由6个属性是对父元素设置的：

- flex-direction: 设置主轴的方向
- justify-content: 设置主轴上的子元素排列方式
-  flex-wrap：设置子元素是否换行
- align-content：设置侧轴上的子元素的排列方式（多行）
- align-items：设置侧轴上的子元素的排列方式（单行）
- flex-flow：复合属性，相当于同时设置了flex-direction和flex-wrap

#### 2.1 flex-direction设置主轴的方向

1、 在flex布局中，分为主轴和侧轴两个方向

- 默认的主轴就是x轴方向，水平向右;
- 默认的侧轴方向就是y轴方向，水平向下。

2、 属性值：

| 属性值         | 说明                                 |
| -------------- | ------------------------------------ |
| row            | 默认从左向右                         |
| row-reverse    | 从右向左                             |
| column         | 主轴设置为y轴，x轴成为侧轴，从上向下 |
| column-reverse | 主轴设置为y轴，x轴成为侧轴，从下至上 |

#### 2.2 justify-content：设置主轴上的子元素的排列方式

注意：**使用这个属性之前一定要确定好主轴是哪一个**

| 属性值        | 说明                                          |
| ------------- | --------------------------------------------- |
| flex-start    | 默认值，从头部开始，如果主轴是x轴，则从左向右 |
| flex-end      | 从尾部开始排列                                |
| center        | 在主轴居中对齐（如果主轴是x轴则水平居中）     |
| space-around  | 平分剩余空间                                  |
| space-between | 先两边贴边，再平分剩余空间                    |

#### 2.3 flex-wrap：设置子元素是否换行

默认情况下，项目都排在一条线上。flex-wrap属性定义，flex布局中默认是不换行的（超过父元素宽度会缩小子元素的宽度）

| 属性值  | 说明   |
| ------- | ------ |
| no-wrap | 不换行 |
| wrap    | 换行   |

#### 2.4 align-content：设置侧轴上的子元素的排列方式（多行）

设置子项在侧轴上的排列方式并且只能用于子项出现 **换行**的情况（多行），**在单行下是没有效果的**

| 属性值        | 说明                                   |
| ------------- | -------------------------------------- |
| flex-start    | 默认值，在侧轴的头部开始排列           |
| flex-end      | 在侧轴的尾部开始排列                   |
| center        | 在侧轴中间显示                         |
| space-around  | 子项在侧轴平分剩余空间                 |
| space-between | 子项在侧轴先分布在两头，再平分剩余空间 |
| stretch       | 设置子项元素高度平分父元素高度         |

#### 2.5 align-items：设置侧轴上的子元素的排列方式（单行）

该属性是控制子项在侧轴（默认是y轴）上的排列方式，在子项为单项（单行）的时候使用

| 属性值     | 说明                     |
| ---------- | ------------------------ |
| flex-start | 默认值，从上到下         |
| flex-end   | 从下到上                 |
| center     | 挤在一起居中（垂直居中） |
| stretch    | 拉伸                     |

#### 2.6 flex-flow：复合属性，相当于同时设置了flex-direction和flex-wrap属性

```css
flex-direction: column;
flex-wrap: wrap;
// 可以简写为：
flex-flow: column wrap;
```

### 三、flex布局子项常见属性

- flex子项目占的份数
- align-self控制子项自己再侧轴的排列方式
- order属性定义子项的排列顺序（前后顺序）

#### 3.1 flex子项目占的份数

flex属性定义子项目分配剩余空间，用flex来表示占多少份数。

**案例1: 左右两侧固定，中间自适应**

```html
<div>
  <section>
    <div></div>
    <div></div>
    <div></div>
  </section>
</div>
```

```css
section {
  width: 60%;
  height: 150px;
  background-color: pink;
  margin: 0 auto;
  display: flex;
}

section div:nth-child(1) {
  width: 100px;
  height: 150px;
  background-color: red;
}

/* 左右两侧固定，中间自适应 */
section div:nth-child(2) {
  flex: 1;
  background-color: green;
}

section div:nth-child(3) {
  width: 100px;
  height: 150px;
  background-color: blue;
}
```

**案例2: 三等份**

```html
<p>
  <span>1</span>
  <span>2</span>
  <span>3</span>
</p>
```

```css
p {
  display: flex;
  width: 60%;
  height: 150px;
  background-color: pink;
  margin: 100px auto;         
}
/* 三等均分 */
p span {
  flex: 1;
}
```

**案例3: 不均分**

```css
p {
  display: flex;
  width: 60%;
  height: 150px;
  background-color: pink;
  margin: 100px auto;
}

p span {
  flex: 1;
}
/* 第二个占2份 */
p span:nth-child(2) {
  flex: 2;
  background-color: skyblue;
}
```

#### 3.2 align-self控制子项自己在侧轴上的排列方式

align-self属性允许单个项目与其它项目不一样的对齐方式，可覆盖align-items属性。

默认值为auto，表示继承父元素的align-items属性，如果没有父元素，则等同于stretch。

案例1:

```html
<div id="box">
  <span>1</span>
  <span>2</span>
  <span>3</span>
</div>
```

```css
#box {
  display: flex;
  width: 80%;
  height: 300px;
  background-color: pink;
}

#box span {
  width: 150px;
  height: 100px;
  background-color: purple;
  margin-right: 5px;
}

#box span:nth-child(3) {
  align-self: flex-end;
}
```

#### 3.3 order属性定义子项的排列顺序（前后顺序）

数值越小，排列越靠前，默认为0。

```css
#box {
  display: flex;
  width: 80%;
  height: 300px;
  background-color: pink;
}

#box span {
  width: 150px;
  height: 100px;
  background-color: purple;
  margin-right: 5px;
}

// 默认是0，-1比0小，则第二个span将提前
#box span:nth-child(2) {
  order: -1;
}

#box span:nth-child(3) {
  align-self: flex-end;
}
```

